# SITE Network Information


This page describes [JP-KEKCC](https://wlcg-cric.cern.ch/core/netsite/detail/JP-KEKCC/) network information for WLCG use.

LAST UPDATE: September 27th, 2024, 11:00 JST (UTC+9)

## Network Overview

[JP-KEK-CRC-02](https://wlcg-cric.cern.ch/core/rcsite/detail/JP-KEK-CRC-02/) is one of the subsystems installed in the KEK Central Computing System (KEKCC). KEKCC is within an isolated network from KEK and J-PARC, as shown in blue and green, respectively, in the figure below.

KEK (ASN1754) connects to [JP-SINET-NREN](https://wlcg-cric.cern.ch/core/rcsite/detail/JP-SINET-NREN/) through two peering with a bandwidth of 100 Gbps primary and a 10 Gbps backup line. All external traffic from/to KEKCC is routed through the central network switch (Nexus 9516), directly peering to the border switch (ARISTA) for [LHCONE](https://wlcg-cric.cern.ch/core/networkroute/detail/JP-KEKCC-LHCONE/) with a bandwidth of 100 Gbps. Also, there is another peer to the border switch (ARISTA) via the firewall (SRX4100) with a bandwidth of 20 Gbps for non-LHCONE network traffic. In addition to the external traffic through SINET above, KEKCC has direct peers to the KEK Campus Network and J-PARC Campus Network.

![KEKCC Network Overview](figs/KEKCC-2024.png "KEKCC Network Overview")

## Network Monitoring

The external traffic from/to KEKCC is monitored on the central network switch (Nexus 9516), available at: https://wlcg-site-traffic-monitor.cc.kek.jp/kekcc-netmon.json, where it is accessible only within KEKCC or from KEK's and CERN's IP addresses such as:

- KEKCC: 202.13.202.0/23, 2001:2f8:3e:cc26::/64
- KEK: 130.87.0.0/16
- CERN: 137.138.0.0/16, 188.184.0.0/15, 2001:2f8:3e::/48, 2001:1458:0D00::/44

The traffic contains all the traffic through LHCONE and non-LHCONE from/to KEKCC, as well as the traffic between KEKCC and KEK/J-PARC Campus Network.

Monitored ports:

All the monitored ports on the KEKCC central network switch (Nexus 9516) are listed below.

- KEKCC - LHCONE:
  - Ethernet1/22: LHCONE primary line (100G)
  - Ethernet12/22: LHCONE backup line (10G)
  - Ethernet2/51: LHCONE reserved/unlinked port (100G)
- KEKCC - SINET through SRX4100:
  - Ethernet2/48: A member of LACP port-channel65 (10G)
  - Ethernet3/48: A member of LACP port-channel65 (10G)
- KEKCC - KEK for KEKCC Campus LAN:
  - Ethernet6/45: A member of LACP port-channel148 (10G)
  - Ethernet7/45: A member of LACP port-channel148 (10G)
  - Ethernet12/23: A reserved/unlinked port
- KEKCC - J-PARC for J-PARC Campus LAN:
  - Ethernet6/47: A member of LACP port-channel149 (10G)
  - Ethernet7/47: A member of LACP port-channel149 (10G)
